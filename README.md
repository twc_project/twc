# TWC

TWC is a largely feature-less Wayland compositor.  It serves as a
demonstration base compositor for the [TWC Project].  It is expected
to be deployed in combination with any number of Built-in Clients in
order to form a custom Desktop Environment.

TWC is based on [libtwc].  It supports Built-in Clients and the [Agent
Protocol].  Built-in Clients may also use the Wayland, xdg_shell and
xdg_decoration protocols.

Built-in Clients are introduced by way of plug-ins.  At startup, a
list of plug-ins is read from a configuration file.  The plug-ins
completely determine the look-and-feel of the TWC server.  Examples of
Built-in Clients can be found in the [TWC plug-ins] repository.

See the [TWC Project] for more context and instructions on how to
build.

[TWC Project]:    https://gitlab.com/twc_project
[libtwc]:         https://gitlab.com/twc_project/libtwc
[Agent Protocol]: https://gitlab.com/twc_project/agent_protocol
[TWC plug-ins]:   https://gitlab.com/twc_project/plug-ins

