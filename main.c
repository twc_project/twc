
  // libtwc
#include <twc/twc_client_initialize.h>  // twc_config_data
#include <twc/twc_server.h>

extern
int
main(
    int   argc,
    char *argv[]
) {
    struct twc_config_data twc_config_data;

    if ( argc == 2 ) {
        twc_config_data = twc_setup( argv[1] );
    } else {
        twc_config_data = twc_setup( "~/.config/twc/twc.toml" );
    }

    twc_load_plugins( &twc_config_data );

    twc_run();

    return 0;
}

// A base compositor must contain all entry points used by plugins
// even if the base compositor doesn't reference them.  Therefore, the
// linker must be told to include all compilation units (.o's) in each
// library.
//
// Fix: Meson is broken.  There is no way to specify that all
// compilation units within a library must be included in an
// executable.  The "link_whole" attribute for the declare_denpendency
// function does not work.
//
// The following code creates artificial dependencies on
//
//   twc_image_file_helper.o    in libtwc-util.a
//   wl_DeVault.o               in libtwc-util.a
//
// If the code in this file gets included,
// so will twc_image_file_helper.o

#include "twc/twc_image_file_helper.h"
extern void meson_gratuitous_reference_0(void);
extern void meson_gratuitous_reference_0(void){twc_jpeg_file_operation(0,NULL,0,0,0,NULL,0);}

#include "twc/wl_DeVault.h"
extern void meson_gratuitous_reference_1(void);
extern void meson_gratuitous_reference_1(void){close_shm_file(0);}
